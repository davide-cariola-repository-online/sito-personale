window.onload = function() {
    document.getElementById('contact-form').addEventListener('submit', function(event) {
        event.preventDefault();
        // generate a five digit number for the contact_number variable
        this.contact_number.value = Math.random() * 100000 | 0;
        // these IDs from the previous steps
        emailjs.sendForm('service_xxx', 'contact_form_dc', this)
            .then(function() {
                console.log('SUCCESS!');
                document.getElementById('flashMessage').innerHTML = `
                    <i class="fas fa-check me-2"></i> Messaggio inviato correttamente!
                    `;
                document.getElementById('contact-form').reset();
            }, function(error) {
                console.log('FAILED...', error);
            }
            );
    });
};
