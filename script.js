// const { EmailJSResponseStatus } = require("emailjs-com");

// NAVBAR SCROLL EFFECT
let navbar = document.getElementById('navbar');

window.onscroll = function () {
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
        navbar.classList.add("nav-bg-down");
        navbar.classList.remove("nav-bg-custom")
    } else {
        navbar.classList.remove("nav-bg-down");
        navbar.classList.add("nav-bg-custom");
        }
}

// TITLE ANIMATION ON SCROLL
function isOnScreen(elem) {

	if( elem.length == 0 ) {
		return;
	}
	var $window = jQuery(window)
	var viewport_top = $window.scrollTop()
	var viewport_height = $window.height()
	var viewport_bottom = viewport_top + viewport_height
	var $elem = jQuery(elem)
	var top = $elem.offset().top
	var height = $elem.height()
	var bottom = top + height

	return (top >= viewport_top && top < viewport_bottom) ||
	(bottom > viewport_top && bottom <= viewport_bottom) ||
	(height > viewport_height && top <= viewport_top && bottom >= viewport_bottom)
}

let sectionPortfolio = document.getElementById('section-portfolio')
let sectionChiSono = document.getElementById('section-chiSono')
let sectionLinguaggi = document.getElementById('section-linguaggi')
let sectionContatti = document.getElementById('section-contatti')
let navPortfolio = document.getElementById('nav-portfolio')
let navChiSono = document.getElementById('nav-chiSono')
let navContatti = document.getElementById('nav-contatti')

jQuery( document ).ready( function() {
	window.addEventListener('scroll', function(e) {
		if( isOnScreen( jQuery( '#portfolio' ) ) ) {
			sectionPortfolio.classList.add("typing-style");
            sectionPortfolio.classList.remove("no-display");
            navPortfolio.classList.add('nav-item-active');
 		} else {
             navPortfolio.classList.remove('nav-item-active')
         }	
	});
});

jQuery( document ).ready( function() {
	window.addEventListener('scroll', function(e) {
		if( isOnScreen( jQuery( '#chiSono' ) ) ) {
			sectionChiSono.classList.add("typing-style2");
            sectionChiSono.classList.remove("no-display");
            navChiSono.classList.add('nav-item-active');
 		}	else {
            navChiSono.classList.remove('nav-item-active')
        }	
	});
});

jQuery( document ).ready( function() {
	window.addEventListener('scroll', function(e) {
		if( isOnScreen( jQuery( '#linguaggi' ) ) ) {
			sectionLinguaggi.classList.add("typing-style3");
            sectionLinguaggi.classList.remove("no-display");
 		}		
	});
});

jQuery( document ).ready( function() {
	window.addEventListener('scroll', function(e) {
		if( isOnScreen( jQuery( '#contatti' ) ) ) {
			sectionContatti.classList.add("typing-style4");
            sectionContatti.classList.remove("no-display");
            navContatti.classList.add('nav-item-active');
 		} else {
            navContatti.classList.remove('nav-item-active')
        }		
	});
});


// SLICK CAROUSEL
jQuery(document).ready(function(){

    $('.items').slick({
    dots: false,
    infinite: true,
    speed: 700,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 4,
    slidesToScroll: 2,
    responsive: [
    {
    breakpoint: 1024,
    settings: {
    slidesToShow: 3,
    slidesToScroll: 2,
    infinite: true,
    dots: false
    }
    },
    {
    breakpoint: 600,
    settings: {
    slidesToShow: 2,
    slidesToScroll: 2
    }
    },
    {
    breakpoint: 480,
    settings: {
    slidesToShow: 1,
    slidesToScroll: 1
    }
    }
    
    ]
    });
    });